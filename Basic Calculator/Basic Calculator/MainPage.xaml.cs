﻿using System;
using Xamarin.Forms;



namespace Basic_Calculator
{
	public partial class MainPage : ContentPage
	{
		double Num1;
		double Num2;
		string math_op;
		int state = 1;
		
		public MainPage()
		{
			InitializeComponent();
			Clear(this, null);
		}
		
		void Num(object sender, EventArgs e)
		{
			Button button = (Button)sender;
			string selected = button.Text;

			if (calc_screen.Text == "0" || state < 0)
			{
				calc_screen.Text = "";
				if (state < 0)
					state = state * -1;
			}

			calc_screen.Text = calc_screen.Text + selected;

			double num;

			if (double.TryParse(calc_screen.Text, out num))
			{
				calc_screen.Text = num.ToString("N0");
				if (state == 1)
				{
					Num1 = num;
				}
				else
				{
					Num2 = num;
				}
			}
		}

		void Operator(object sender, EventArgs e)
		{
			state = -2;

			Button button = (Button)sender;
			
			string selected = button.Text;

			math_op = selected;
		}

		void Clear(object sender, EventArgs e)
		{
			Num1 = 0;

			Num2 = 0;

			state = 1;

			calc_screen.Text = "";
		}

		void Calc(object sender, EventArgs e)
		{
			if (state == 2)
			{
				double result = 0;
				if (math_op == "+")
				{ result = (double)Num1 + Num2; }
				if (math_op == "-")
				{ result = (double)Num1 - Num2; }
				if (math_op == "x")
				{ result = (double)Num1 * Num2; }
				if (math_op == "÷")
				{ result = (double)Num1/Num2; }
				calc_screen.Text = result.ToString();
				Num1 = result;
				state = -1;
			}
		}
	}
}